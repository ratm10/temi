
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'memberProvier.dart';

class MatchScheduleScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final memberProvider = Provider.of<MemberProvider>(context);
    final schedule = memberProvider.createMatchSchedule();


    return Scaffold(
      appBar: AppBar(
        title: Text('Match Schedule'),
      ),
      body: ListView.builder(
        itemCount: schedule.length,
        itemBuilder: (context, index) {
          final match = schedule[index];
          return ListTile(
            title: Text(
              '${match["team1"]![0].name} (${match["team1"]![0].skill}) & ${match["team1"]![1].name} (${match["team1"]![1].skill}) vs ${match["team2"]![0].name} (${match["team2"]![0].skill}) & ${match["team2"]![1].name} (${match["team2"]![1].skill})',
            ),
          );
        },
      ),
    );
  }
}