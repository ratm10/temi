import 'package:cloud_firestore/cloud_firestore.dart';

class CounterService {
  static final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  static final String _counterDoc = 'counters/sequence';

  static Future<int> getNextSequence() async {
    return _firestore.runTransaction((transaction) async {
      DocumentReference counterRef = _firestore.doc(_counterDoc);
      DocumentSnapshot counterSnapshot = await transaction.get(counterRef);

      if (!counterSnapshot.exists) {
        transaction.set(counterRef, {'value': 1});
        return 1;
      }

      int newValue = (counterSnapshot.data() as Map<String, dynamic>)['value'] + 1;
      transaction.update(counterRef, {'value': newValue});

      return newValue;
    });
  }
}