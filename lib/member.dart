import 'package:cloud_firestore/cloud_firestore.dart';

class Member {
  int id;
  String name;
  String birth;
  String skill;
  DateTime? updatedAt;

  Member({required this.id, required this.name, required this.birth, required this.skill, this.updatedAt});


  Map<String, dynamic> toMap() {
    return {
      'name': name,
      'birth': birth,
      'skill': skill,
      'updated_at': FieldValue.serverTimestamp(),
    };
  }

  // Firestore로부터 데이터를 받아올 때 사용할 메서드
  factory Member.fromMap(Map<String, dynamic> map) {
    return Member(
      id: map['id'],
      name: map['name'],
      birth: map['birth'],
      skill: map['skill'],
      updatedAt: (map['updated_at'] as Timestamp).toDate(),
    );
  }

  @override
  String toString() {
    return 'Member{name: $name}';
  }
}