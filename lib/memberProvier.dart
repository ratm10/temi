import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:temi/counterService.dart';

import 'main.dart';
import 'member.dart';

class MemberProvider with ChangeNotifier {
  List<Member> _members = [];

  Map<String, int> gameCountMap = {};

  List<Member> get members => _members;

/*  void addMember(String name, String skill) {
    //_members.add(Member(name, skill));
    notifyListeners();
  }  */


  Future<bool> addMember({required String name, required String birth, required String skill}) async {

    String docId = await getUniqueDocId(name);
    int id = await CounterService.getNextSequence();

    try{
      Member member = Member(id: id, name: name, birth: birth, skill: skill);
      _members.add(member);

      var map = {
        'id' : id,
        'name' : name,
        'birth' : birth,
        'skill': skill,
        'updated_at': FieldValue.serverTimestamp(),
      };

      await FirebaseFirestore.instance.collection('members').doc(docId).set(map);
      notifyListeners();
      return true;
    } catch (e) {
      print('Error adding member: $e');
      return false;
    }

    /*.then((value) {
      notifyListeners();
    }).catchError((error) {
      print('Failed to add user: $error');
    });*/
  }

  Future<String> getUniqueDocId(String name) async {
    String docId = name;
    int counter = 1;
    var doc = await FirebaseFirestore.instance.collection('members').doc(docId).get().catchError((error) {
      print('getUniqueDocId error: $error');
    });
    while (doc.exists) {
      docId = '$name$counter';
      doc = await FirebaseFirestore.instance.collection('members').doc(docId).get();
      counter++;
    }
    return docId;
  }

  void addMemberToMatch({required String name, required String skill}) {
   // _members.add(Member(name, skill));

    FirebaseFirestore.instance.collection('members').doc('member info').set({
      'id' : name,
      'name' : name,
      'skill': skill,
      'updated_at': FieldValue.serverTimestamp(), // 서버 타임스탬프 사용
    }).then((value) {
      //print('User Added with ID: ${value.id}');
    }).catchError((error) {
      print('Failed to add user: $error');
    });
  }


  List<List<Member>> createTeams() {
    _members.add(Member(id: 1, name: '김성천', birth: '8610', skill: '1'));
    _members.add(Member(id: 2, name: '장세아', birth: '9505', skill: '2'));
    _members.add(Member(id: 3, name: '홍길동', birth: '8610', skill: '3'));
    _members.add(Member(id: 4, name: '김앤장', birth: '8610', skill: '4'));
    _members.add(Member(id: 5, name: '테스트', birth: '8610', skill: '5'));

    List<List<Member>> listOfLists = [];

    for (var i = 0; i < _members.length; i++) {
      for (var j = i + 1; j < _members.length; j++) {
        //teams.add([_members[i], _members[j]]);



        addToUniqueList(listOfLists, [_members[i], _members[j]]);
      }
    }
    listOfLists.sort((a, b) => (a[0].skill.codeUnitAt(0) - a[1].skill.codeUnitAt(0)).abs()
        .compareTo((b[0].skill.codeUnitAt(0) - b[1].skill.codeUnitAt(0)).abs()));


    //listOfLists.sort((a, b) => (a[0].skill.codeUnitAt(0) + a[1].skill.codeUnitAt(0)).compareTo(b[0].skill.codeUnitAt(0) + b[1].skill.codeUnitAt(0)));

    return listOfLists.reversed.toList();
  }

  List<Map<String, List<Member>>> createMatchSchedule() {
    List<List<Member>> teams = createTeams();

    List<Map<String, List<Member>>> schedule = [];
    for (var i = 0; i < teams.length; i++) {

      for (var j = i + 1; j < teams.length; j++) {
        if (teams[i].toSet().intersection(teams[j].toSet()).isEmpty) {

          /*for (var member in teams[i]) {
            gameCountMap.containsKey(member.name) ? gameCountMap[member.name] = gameCountMap[member.name]! + 1 : gameCountMap[member.name] = 1;
          }

          for (var member in teams[j]) {
            gameCountMap.containsKey(member.name) ? gameCountMap[member.name] = gameCountMap[member.name]! + 1 : gameCountMap[member.name] = 1;
          }*/


          
            checkCombination({"team1": teams[i], "team2": teams[j]});


            schedule.add({"team1": teams[i], "team2": teams[j]});

        }
      }
    }

    print(gameCountMap.toString());

    return schedule;
  }


  void addToUniqueList(List<List<Member>> listOfLists, List<Member> members) {
    // Set을 사용하여 중복 체크
    Set<Member> setOfMembers = Set.from(members);

    print('check: ' + members.toString());


    // 이미 존재하는 조합인지 확인
    if (!listOfLists.any((list) => Set.from(list).containsAll(setOfMembers))) {
      listOfLists.add(members);
    }
  }

  void checkCombination(Map<String, List<Member>> map) {}
}