
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog_null_safe/progress_dialog_null_safe.dart';
import 'package:provider/provider.dart';

import 'mathScheduleScreen.dart';
import 'memberProvier.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}



class _HomeScreenState extends State<HomeScreen> {
  final nameController = TextEditingController();
  final birthController = TextEditingController();
  final skillController = TextEditingController();
  String selectedSkill = '1';
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    final memberProvider = Provider.of<MemberProvider>(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Tennis Doubles Draw'),
      ),
      body: Form(
        key: _formKey,
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(8.0),
              child: TextFormField(
                controller: nameController,
                decoration: InputDecoration(labelText: 'Name *'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Name is required';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: TextField(
                controller: birthController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(labelText: 'Birth(YYmm)'),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(8.0),
              child: DropdownButton<String>(
                value: selectedSkill,
                onChanged: (String? newValue) {
                  setState(() {
                    selectedSkill = newValue!;
                  });
                },
                items: <String>['1', '2', '3', '4', '5']
                    .map<DropdownMenuItem<String>>((String value) {
                  return DropdownMenuItem<String>(
                    value: value,
                    child: Text(value),
                  );
                }).toList(),
              ),
            ),
            ElevatedButton(
              onPressed: () async {
                if (_formKey.currentState!.validate()) {
                  bool result;
                  ProgressDialog progressDialog = ProgressDialog(context);
                  progressDialog.style(message: '회원정보 저장중...');
                  await progressDialog.show();
                  result = await memberProvider.addMember(name: nameController.text, birth: birthController.text, skill: selectedSkill);
                  await progressDialog.hide();
                  showSuccessDialog(result);
                  resetValues();
                }

              },
              child: Text('Add Member'),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: memberProvider.members.length,
                itemBuilder: (context, index) {
                  final member = memberProvider.members[index];
                  return ListTile(
                    title: Text('${member.name} (${member.skill})'),
                  );
                },
              ),
            ),
            ElevatedButton(
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => MatchScheduleScreen()),
                );
              },
              child: Text('Create Match Schedule'),
            ),
          ],
        ),
      )
    );
  }

  void resetValues() {
    setState(() {
      selectedSkill = '1';
      nameController.clear();
      birthController.clear();
      skillController.clear();
    });
  }


  void showSuccessDialog(result) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(result ? 'Success' : 'Fail'),
          content: Text(result ? 'Member added successfully!' : 'Member add fail'),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

}